import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { DestinosViajesState } from './../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import {AppState} from './../app.module'

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates:string[];
  all;

  constructor(
      private destinosApiClient:DestinosApiClient,
      private store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit() {
    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push("Se eligió: " + d.nombre);
        }
      });
      this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  agregado(d:DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d:DestinoViaje) {
    this.destinosApiClient.elegir(d);
  }

}
